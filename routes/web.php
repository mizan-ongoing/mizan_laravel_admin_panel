<?php

use Illuminate\Support\Facades\Route;

/*New routing system*/
//for frontend controller
use App\Http\Controllers\FrontController;
use App\Http\Controllers\DashboardController;
//For Category Controller
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function () {
    return view('admin.login.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




///Controller route create for home page
///Route::get("dashboard", [DashboardController::class,'index']);


///New code for middleware
//Route::get("dashboard", [DashboardController::class,'index'])->middleware('AuthenticateMiddleware');



//New code for Group middleware
Route::group(['middleware'=>'AuthenticateMiddleware'], function(){
	Route::get("dashboard", [DashboardController::class,'index']);
});

///Controller route create for home page
Route::get("front", [FrontController::class,'index']);

//Category route crete 
Route::get('/category/entry', [App\Http\Controllers\CategoryController::class, 'index']);

//Category form data insert
Route::post('/category/entry', [App\Http\Controllers\CategoryController::class, 'save']);

//Category form data manage
Route::get('/category/manage', [App\Http\Controllers\CategoryController::class, 'manage']);