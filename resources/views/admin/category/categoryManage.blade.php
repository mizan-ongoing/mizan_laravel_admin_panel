@extends('admin.master')


@section('website-title')
	Category
@endsection


@section('content-heading')
	Category
@endsection


@section('page-heading')
	Category
@endsection


@section('page-title')
	Category Manage
@endsection





@section('mainContent')
<!-- 
      @foreach($category as $singleCategory)
        {{ $singleCategory->categoryName }}
      @endforeach
 -->
    
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Category Manage
              </header>
              <div class="panel-body">




                <div class="row">
                  <div class="col-lg-12">
                    <section class="panel">
                      <header class="panel-heading">
                        All category information
                      </header>
                      <table class="table table-striped table-advance table-hover">
                        <tbody>
                          <tr>
                            <th><i class="fa fa-list-alt"></i> Serial No.</th>
                            <th><i class="icon_profile"></i>Name</th>
                            <th><i class="icon_calendar"></i>Description</th>
                            <th><i class="icon_pin_alt"></i>Publication Status</th>
                            <th><i class="icon_cogs"></i> Action</th>
                          </tr>
                          <?php
                            $i=0;
                          ?>
                          @foreach($category as $singleCategory)
                          <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $singleCategory->categoryName }}</td>
                            <td>{{ $singleCategory->shortDescription }}</td>
                            <td>{{ ($singleCategory->shortDescriptions == 1)? 'Published' : 'Unpublished' }}</td>
                            <td>
                              <div class="btn-group">
                                <a class="btn btn-primary" href="#"><i class="icon_plus_alt2"></i></a>
                                <a class="btn btn-success" href="#"><i class="icon_check_alt2"></i></a>
                                <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>





              </div>
            </section>
          </div>
        </div>






@endsection