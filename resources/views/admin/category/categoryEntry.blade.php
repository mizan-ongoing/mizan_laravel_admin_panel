@extends('admin.master')


@section('website-title')
	Category
@endsection


@section('content-heading')
	Category
@endsection


@section('page-heading')
	Category
@endsection


@section('page-title')
	Category Entry
@endsection





@section('mainContent')



        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Create New Categoroy
              </header>
              <div class="panel-body">


  {{ Session::get('message') }}



                <!-- <form class="form-horizontal " method="get"> -->
                {!! Form::open(['url' => 'category/entry', 'method'=>'post', 'class'=>'form-horizontal']) !!}
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Category Name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="name">
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="control-label col-sm-3">Shrot Description</label>
                      <div class="col-sm-9">
                        <textarea class="form-control ckeditor"  rows="6" name="shortDescription"></textarea>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-lg-3" for="inputSuccess">Publication Status</label>
                    <div class="col-lg-9">
                      <select class="form-control m-bot15" name="publicationStatus">
                           <option value="1">Published</option>
                           <option value="0">Unpublished</option>

                       </select>
                    </div>
                  </div>



                  <br/>
                  <center><button type="submit" value="Submit" class="btn btn-primary">Submit</button></center>
                <!-- </form> -->
                {!! Form::close() !!}



              </div>
            </section>
          </div>
        </div>






@endsection