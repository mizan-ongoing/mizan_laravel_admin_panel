<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Login Page 2 | Creative - Bootstrap 3 Responsive Admin Template</title>

  <!-- Bootstrap CSS -->
  <link href="{{ asset('admin') }}/css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="{{ asset('admin') }}/css/elegant-icons-style.css" rel="stylesheet" />
  <link href="{{ asset('admin') }}/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="{{ asset('admin') }}/css/style.css" rel="stylesheet">
  <link href="{{ asset('admin') }}/css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->
</head>

<body class="login-img3-body">

  <div class="container">









    <!-- <form class="login-form" action="index.html"> -->

    <form class="login-form" method="POST" class="login-form" action="{{ route('login') }}" >
                        @csrf

      <div class="login-wrap">

<center><a href="{{ url('/front') }}" class="btn btn-info" style=" background-color: #33cc99; border-radius: 20px; padding: 10px;"><b>Frontend View</b></a></center>

        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_profile"></i></span>
          <input type="text" class="form-control" placeholder="Email" name="email" autofocus>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-primary" style=" background-color: #00aaff; border-radius: 20px; padding: 10px;">
             {{ __('Login') }}
         </button>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-info" style=" background-color: #ff80ed; border-radius: 20px; padding: 10px;"><b>Register</b></a>
                        @endif



        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif

        <!-- <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
        <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button> -->
      </div>
    </form>













  </div>


</body>

</html>

