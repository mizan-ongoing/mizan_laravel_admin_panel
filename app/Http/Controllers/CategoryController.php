<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//for model add
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.category.categoryEntry');
    }


    public function save(Request $request)
    {
        //dd($request->all());
        //eloquent orm
        $categoryEntry =  new Category();

        $categoryEntry->categoryName = $request->name;
        $categoryEntry->shortDescription = $request->shortDescription;
        $categoryEntry->publicationStatus = $request->publicationStatus;
        //$categoryEntry->categoryName = $request->name;

        $categoryEntry->save();
        return redirect('/category/entry')->with('message','Data insert successfully');
    }



    public function manage()
    {
        ///All categories get from database
        $categories =  Category::all();

        return view('admin.category.categoryManage',['category'=>$categories]);
    }


}
